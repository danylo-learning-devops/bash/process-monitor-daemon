#!/bin/bash

# Process daemon. Monitor all process. Once a $runInterval send it to $adminEmail and write to $logFileProcess
# store PID in $pidFile, store all inputed commands in $logFile
# Required: bash, ssmtp

daemonName="process-daemon"
pidDir="."
pidFile="$daemonName.pid"
logDir="."
logFile="$logDir/$daemonName.log"
logFileProcess="$logDir/logFileProcess.log"
tempFileForEmail="$logDir/tempFileForEmail.txt"
runInterval=60*10 #in seconds
myPid=$(echo $$)
adminEmail="danylo.mysko@gmail.com"

function doCommands() {

    declare -a processArray
    processArray=( $( ps -eo pid) )
    processArray=("${processArray[@]:1}")
    dateNow=$(date "+%H:%M:%S %d/%m/%y")
    
    workWithLogs "prepearMail"

    workWithLogs "writeToFiles" " "
    workWithLogs "writeToFiles" "***New system scan"
    workWithLogs "writeToFiles" "Start scanning system at $dateNow"

    for (( i=0; i < ${#processArray[@]}; i++ ))
    do
        nowWeCheck=$(pgrep -P ${processArray[i]} | wc -w)
        if [[ $nowWeCheck -gt 5 ]]
        then
            processName=$(ps -p ${processArray[i]} -o comm=)
            workWithLogs "writeToFiles" "Process $processName with PID ${processArray[i]} get more then 5 childs: $nowWeCheck"
        fi
    done

    dateFinish=$(date "+%H:%M:%S %d/%m/%y")
    workWithLogs "writeToFiles" "Scan finish at $dateFinish"
    workWithLogs "writeToFiles" "***Scan finish"

    workWithLogs "sendLogsToEmail"

}

function workWithLogs() {

    if [[ "$1" = "prepearMail" ]]; then
        touch "$tempFileForEmail"
        echo "$adminEmail" >> "$tempFileForEmail"
        echo "From: $adminEmail" >> "$tempFileForEmail"
        echo "Subject: Logs" >> "$tempFileForEmail"
    fi

    if [[ "$1" = "sendLogsToEmail" ]]; then
        ssmtp $adminEmail < $tempFileForEmail
        _deleteTempEmailFIle
    fi

    if [[ "$1" = "writeToFiles" ]]; then
    	#Usage: $2 is writed to $logFileProcess and $tempFileForEmail
    	if [ "$2" ]; then
    		echo "$2" | tee --append "$logFileProcess" "$tempFileForEmail" > "/dev/null"
    	fi
    fi

    if [ "$1" = "writeToCommandsLog" ]; then
    	if [ "$2" ]; then
    		echo "$2" >> "$logFile"
    	fi
    fi

    function _deleteTempEmailFIle() {
        rm $tempFileForEmail
    }

}

function setupDaemon() {

    if [ ! -f "$logFile" ]; then
     touch "$logFile"
    fi

    if [ ! -f "$logFileProcess" ]; then
     touch "$logFileProcess"
    fi

}

function checkDaemon() {

    if [ -z "$oldPid" ]; then
        return 0
    elif [[ $(ps aux | grep "$oldPid" | grep "$daemonName" | grep -v grep) > /dev/null ]]; then
        if [ -f "$pidFile" ]; then
            if [[ $(cat "$pidFile") -eq "$oldPid" ]]; then
                # Daemon is running.
                return 1
            else
                # Daemon isn't running.
                return 0
            fi
        else
            #TODO check PID 
            return 1
        fi
        return 1
    fi

 }

function loop() {

	doCommands
	sleep $runInterval
	loop

}

function stopDaemon() {

    checkDaemon
    local daemonAlive=$?
    if [[ "$daemonAlive" = 0 ]]; then
        echo " * Error: $daemonName is not running."
        exit 1
    fi
    echo " * Stopping $daemonName"
    workWithLogs "writeToCommandsLog" "*** "$(date +"%Y-%m-%d")": $daemonName stopped."

    if [[ ! -z $(cat "$pidFile") ]]; then
        kill -9 $(cat "$pidFile") &> /dev/null
    fi

}

function startDaemon() {

    # Start the daemon.
    setupDaemon
    checkDaemon
    local daemonAlive=$?
    if [[ "$daemonAlive" = 1 ]]; then
        echo "Error: $daemonName is already running."
        exit 1
    fi
    echo " * Starting $daemonName with PID: $myPid."
    echo "$myPid" > "$pidFile"

    workWithLogs "writeToCommandsLog" "*** "$(date +"%Y-%m-%d")": Starting up $daemonName."

    # Start the loop.
    loop

}

function statusDaemon() {

    checkDaemon
    local daemonAlive=$?
    if [[ "$daemonAlive" = 1 ]]
    then
        echo " * $daemonName is running."
    else
        echo " * $daemonName isn't running."
    fi
        exit 0

}

function restartDaemon() {

	checkDaemon
	local daemonAlive=$?
	if [[ "$daemonAlive" = 0 ]]; then
	    echo "$daemonName isn't running."
	    exit 1
	fi
	stopDaemon
	startDaemon

}

#Commands input
if [ -f "$pidFile" ]; then
    oldPid=$(cat "$pidFile")
fi
checkDaemon
case "$1" in
start)
    startDaemon
;;
stop)
    stopDaemon
;;
status)
    statusDaemon
;;
restart)
    restartDaemon
;;
*)
echo "Error: usage $0 { start | stop | restart | status }"
exit 1
esac

exit 0